let express = require("express");
let app = express();
const mysql = require("mysql");
var cors = require("cors");
var nodemailer = require("nodemailer");
const bodyParser = require("body-parser");
var path=require("path");
require("./models/models");
var PartRouter=require("./controller/DetailOrder");
var CustomerRouter=require("./controller/Customer");
var RoomRouter=require("./controller/Rooms");
var ServiceRouter=require("./controller/Services");
var OrderRouter=require("./controller/OrderRoom")
app.use(cors());
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(bodyParser.json({limit:"50mb"}));
app.all('/*', function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', "*");
	res.setHeader('Access-Control-Allow-Credentials', true);
  	res.setHeader('Access-Control-Allow-Headers', "Origin, X-Requested-With, Content-Type, Accept");
  	res.setHeader('Access-Control-Allow-Methods','GET,PUT,POST,DELETE,OPTIONS');
  	next();
});
app.use("/detail-order",PartRouter);
app.use("/customers",CustomerRouter);
app.use("/rooms",RoomRouter);
app.use("/service",ServiceRouter);
app.use("/order",OrderRouter);
app.get('/', function(req, res){
  res.json("welcome to API");
  //res.end();
});
app.listen(8080, function(){
  console.log('listening on *:'+8080);
});