"use strict";
var express = require("express");
var router = express.Router();
router.get("/get-all", (req, res) => {
  DetailOrderTickets.findAll({
    attributes: [
      "id",
      "amount_room",
      "order_ticket_id",
      "rank_room_id",
      "createdAt",
      "updatedAt",
    ],
    include: [
      {
        required: true,
        model: OrderTickets,
        include: [
          {
            required: true,
            model: Customers,
            as: "detail_info_customer",
          },
          {
            required: true,
            model: Staffs,
            include: [
              {
                required: true,
                model: Part,
                as: "detail_info_part",
              },
            ],
            as: "detail_info_staff",
          },
        ],
        as: "detail_info_order",
      },
      {
        required: true,
        model: RankRooms,
        include: [
          {
            required: true,
            model: KindRooms,
            as: "detail_info_kind_room",
          },
          {
            required: true,
            model: TypeRooms,
            as: "detail_info_type_room",
          },
        ],
        as: "detail_info_rank_room",
      },
    ],
  })
    .then((detailOrder) => {
      res.json({
        total:detailOrder.length,
        data:detailOrder
      });
      //res.end();
    })
    .catch((err) => console.log(err));
});
module.exports = router;
