"use strict";
var express = require("express");
var router = express.Router();
function randomCode() {
  return (
    "PD" +
    Math.floor(Math.random() * 101) +
    "" +
    Math.floor(Math.random() * 101) +
    "" +
    Math.floor(Math.random() * 101) +
    "" +
    Math.floor(Math.random() * 101) +
    "" +
    Math.floor(Math.random() * 101) +
    "" +
    Math.floor(Math.random() * 101) +
    "" +
    Math.floor(Math.random() * 101) +
    "" +
    Math.floor(Math.random() * 101) +
    "" +
    Math.floor(Math.random() * 101)
  );
}
//đặt phòng
router.post("/order-room", (req, res) => {
  let orderIdTemp = "";
  let {
    idCustomer,
    codeCustomer,
    firtName,
    lastName,
    gender,
    address,
    phone,
    email,
    amountRoom,
    dateWelcome,
    dateLeave,
    status,
    RankRoom
  } = req.body;
  Customers.findAll({
    where: {
      code: codeCustomer,
    },
  }).then((customers) => {
    if (customers.length > 0) {
      OrderTickets.create({
        code: randomCode(),
        date_order: new Date(),
        amount_room: amountRoom,
        date_welcome: dateWelcome,
        date_leave: dateLeave,
        status: status,
        customer_id: customers[0].id
      }).then(result => {
        let arrayDetail = [];
        orderIdTemp = result.id;
        for (let i = 0; i < RankRoom.length; i++) {
          arrayDetail.push({
            order_ticket_id: result.id,
            rank_room_id: RankRoom[i].rankRoomId,
            amount_room: RankRoom[i].amount
          })
        }
        DetailOrderTickets.bulkCreate(arrayDetail).then(resultOrder => {
          //res.json(resultOrder)
          OrderTickets.findOne({
            attributes: [
              "id",
              "code",
              "date_order",
              "amount_room",
              "date_welcome",
              "date_leave",
              "status",
              "createdAt",
              "updatedAt"
            ],
            include: [
              {
                model: Customers,
                as: "detail_info_customer",
              },
              {
                model: DetailOrderTickets,
                as: "detail_list_detail_order",
              },
            ],
            where: {
              id: orderIdTemp
            }
          }).then(result => res.json(result))
          //update status
          let arrStatusRoom = [];
          for (let i = 0; i < RankRoom.length; i++) {
            let temp = RankRoom[i].listRoom;
            for (let j = 0; j < temp.length; j++) {
              arrStatusRoom.push({
                time_start: dateWelcome,
                time_end: dateLeave,
                rooms_id: temp[j],
                status_rooms_id: 1,
                order_tickets_id1: orderIdTemp
              })
            }
          }
          DetailStatus.bulkCreate(arrStatusRoom).then(a => console.log("status", a))
        })
      })
      Customers.update(
        {
          firt_name: firtName,
          last_name: lastName,
          gender: gender,
          address: address,
          phone_number: phone,
          email: email
        },
        {
          where: {
            code: codeCustomer,
          },
        }
      );
    }
    else {
      Customers.create({
        code: codeCustomer,
        firt_name: firtName,
        last_name: lastName,
        gender: gender,
        address: address,
        phone_number: phone,
        email: email
      }).then(result => {
        OrderTickets.create({
          code: randomCode(),
          date_order: new Date(),
          amount_room: amountRoom,
          date_welcome: dateWelcome,
          date_leave: dateLeave,
          status: status,
          customer_id: result.id
        }).then(result => {
          let arrayDetail = [];
          orderIdTemp = result.id;
          for (let i = 0; i < RankRoom.length; i++) {
            arrayDetail.push({
              order_ticket_id: result.id,
              rank_room_id: RankRoom[i].rankRoomId,
              amount_room: RankRoom[i].amount
            })
          }
          DetailOrderTickets.bulkCreate(arrayDetail).then(resultOrder => {
            OrderTickets.findOne({
              attributes: [
                "id",
                "code",
                "date_order",
                "amount_room",
                "date_welcome",
                "date_leave",
                "status",
                "createdAt",
                "updatedAt"
              ],
              include: [
                {
                  model: Customers,
                  as: "detail_info_customer",
                },
                {
                  model: DetailOrderTickets,
                  as: "detail_list_detail_order",
                },
              ],
              where: {
                id: orderIdTemp
              }
            }).then(result => res.json(result))
            let arrStatusRoom = [];
            for (let i = 0; i < RankRoom.length; i++) {
              let temp = RankRoom[i].listRoom;
              for (let j = 0; j < temp.length; j++) {
                arrStatusRoom.push({
                  time_start: dateWelcome,
                  time_end: dateLeave,
                  rooms_id: temp[j],
                  status_rooms_id: 1,
                  order_tickets_id1: orderIdTemp
                })
              }
            }
            DetailStatus.bulkCreate(arrStatusRoom).then(a => console.log("status", a))
          })
        })
      })
    }
  });
});
//lấy ra phiếu đặt
router.get("/get-order-ticket/:id", (req, res) => {
  let { id } = req.params;
  OrderTickets.findOne({
    attributes: [
      "id",
      "code",
      "date_order",
      "amount_room",
      "date_welcome",
      "date_leave",
      "status",
      "createdAt",
      "updatedAt"
    ],
    include: [
      {
        model: Customers,
        as: "detail_info_customer",
      },
      {
        model: DetailOrderTickets,
        as: "detail_list_detail_order",
      },
    ],
    where: {
      id: id
    }
  }).then(result => res.json(result))
})
module.exports = router