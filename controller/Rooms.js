"use strict";
var express = require("express");
var router = express.Router();
router.get("/getall", (req, res) => {
  Rooms.findAll({
    attributes: [
      "id",
      "code",
      "floor",
      "rank_room_id",
      "createdAt",
      "updatedAt",
    ],
    include: [
      {
        model: RankRooms,
        as: "detail_info_rankroom",
        include: [
          {
            model: KindRooms,
            as: "detail_info_kind_room",
          },
          {
            model: TypeRooms,
            as: "detail_info_type_room",
          },
        ],
      },
    ],
  }).then((room) => {
    res.json({
      total: room.length,
      data: room,
    });
    //res.end();
  });
});
router.get("/getall/:id", (req, res) => {
  let { id } = req.params;
  Rooms.findAll({
    attributes: [
      "id",
      "code",
      "floor",
      "rank_room_id",
      "createdAt",
      "updatedAt",
    ],
    include: [
      {
        model: RankRooms,
        as: "detail_info_rankroom",
        include: [
          {
            model: KindRooms,
            as: "detail_info_kind_room",
          },
          {
            model: TypeRooms,
            as: "detail_info_type_room",
          },
        ],
      },
    ],
    where: {
      id: id,
    },
  }).then((room) => {
    res.json({
      total: room.length,
      data: room,
    });
    //res.end();
  });
});
router.get("/rank-room/:idRank", (req, res) => {
  let { idRank } = req.params;
  RankRooms.findAll({
    attributes: [
      "id",
    ],
    include: [
      {
        required: true,
        model: KindRooms,
        as: "detail_info_kind_room",
      },
      {
        required: true,
        model: TypeRooms,
        as: "detail_info_type_room",
      },
    ],
    where: {
      id: idRank
    }
  }).then(result => res.json(result))
})
router.get("/rank-room", (req, res) => {
  RankRooms.findAll({
    attributes: [
      "id",
      "code",
      "price"
    ],
    include: [
      {
        required: true,
        model: KindRooms,
        as: "detail_info_kind_room",
      },
      {
        required: true,
        model: TypeRooms,
        as: "detail_info_type_room",
      },
    ],
  }).then(result => res.json({
    total: result.length,
    data: result
  }))
})
router.get("/getby-rank-room/:rankRoomId/:timeStart/:timeEnd", async (req, res) => {
  let { rankRoomId, timeStart, timeEnd } = req.params;
  let sql = `call load_rooms_empty(${rankRoomId},'${timeStart}','${timeEnd}')`
  const results = await sequelize.query(sql);
  res.json({
    total: results.length,
    data: results
  })
});
module.exports = router;
