const {Sequelize}=require("sequelize");
var StatusRooms=sequelize.define('status_rooms',{
    id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
		autoIncrement: true,
		allowNull: false,
    },
    code: {
		type: Sequelize.STRING(255),
		field: 'code',
		unique: true,
		allowNull: false,
    },
    name: {
		type: Sequelize.STRING(255),
		field: 'name',
		allowNull: true,
	},
	createdAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
	updatedAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
},{
    freezeTableName: true,
	indexes: []
});
module.exports=StatusRooms;