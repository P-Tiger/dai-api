const {Sequelize}=require("sequelize");
var DetailStatus=sequelize.define('detail_status',{
    id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
		autoIncrement: true,
		allowNull: false,
    },
    time_start:{
		type:Sequelize.DATE,
		field:"time_start",
		allowNull: true,
	},
	time_end:{
		type:Sequelize.DATE,
		field:"time_end",
		allowNull: true,
	},
	createdAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
	updatedAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
},{
    freezeTableName: true,
	indexes: []
});
module.exports=DetailStatus;