const {Sequelize}=require("sequelize");
var RankRooms=sequelize.define('rank_rooms',{
    id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
		autoIncrement: true,
		allowNull: false,
    },
    code: {
		type: Sequelize.STRING(255),
		field: 'code',
		unique: true,
		allowNull: false,
    },
    price: {
		type: Sequelize.BIGINT,
		field: 'price',
		allowNull: true,
    },
    time:{
		type:Sequelize.DATE,
		field:"time",
		allowNull: true,
	},
	createdAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
	updatedAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
},{
    freezeTableName: true,
	indexes: []
});
module.exports=RankRooms;