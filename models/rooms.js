const {Sequelize}=require("sequelize");
var Rooms=sequelize.define('rooms',{
    id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
		autoIncrement: true,
		allowNull: false,
    },
    code: {
		type: Sequelize.STRING(255),
		field: 'code',
		unique: true,
		allowNull: false,
    },
    floor: {
		type: Sequelize.BIGINT,
		field: 'floor',
		allowNull: true,
    },
	createdAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
	updatedAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
},{
    freezeTableName: true,
	indexes: []
});
module.exports=Rooms;