const {Sequelize}=require("sequelize");
var Bills=sequelize.define('bills',{
    id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
		autoIncrement: true,
		allowNull: false,
    },
    code: {
		type: Sequelize.STRING(255),
		field: 'code',
		unique: true,
		allowNull: true,
    },
    price_room: {
		type: Sequelize.BIGINT,
		field: 'price_room',
		allowNull: true,
    },
    price_service: {
		type: Sequelize.BIGINT,
		field: 'price_service',
		allowNull: true,
    },
    sum_price: {
		type: Sequelize.BIGINT,
		field: 'sum_price',
		allowNull: true,
    },
	createdAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
	updatedAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
},{
    freezeTableName: true,
	indexes: []
});
module.exports=Bills;