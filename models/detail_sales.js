const {Sequelize}=require("sequelize");
var DetailSales=sequelize.define('detail_sales',{
	id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
		autoIncrement: true,
		allowNull: false,
    },
    ratio: {
		type: Sequelize.BIGINT,
		field: 'ratio',
		//unique: true,
		allowNull: true,
    },
	createdAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
	updatedAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
},{
    freezeTableName: true,
	indexes: []
});
module.exports=DetailSales;