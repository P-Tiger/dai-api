const {Sequelize}=require("sequelize");
var DetailCustomerAts=sequelize.define('detail_customer_ats',{
    id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
		autoIncrement: true,
		allowNull: false,
    },
    note: {
		type: Sequelize.STRING(255),
		field: 'note',
		//unique: true,
		allowNull: true,
    },
	createdAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
	updatedAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
},{
    freezeTableName: true,
	indexes: []
});
module.exports=DetailCustomerAts;