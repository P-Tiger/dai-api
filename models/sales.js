const {Sequelize}=require("sequelize");
var Sales=sequelize.define('sales',{
    id: {
        type: Sequelize.BIGINT,
        field: 'id',
        primaryKey: true,
		autoIncrement: true,
		allowNull: false,
    },
    code: {
		type: Sequelize.STRING(255),
		field: 'code',
		unique: true,
		allowNull: false,
    },
    name: {
		type: Sequelize.STRING(255),
		field: 'name',
		allowNull: true,
    },
    date_start:{
		type:Sequelize.DATE,
		field:"date_start",
		allowNull: true,
	},
	date_end:{
		type:Sequelize.DATE,
		field:"date_end",
		allowNull: true,
	},
    createdAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
	updatedAt:{
		type:Sequelize.DATE,
		field:"createdAt",
		allowNull: true,
	},
},{
    freezeTableName: true,
	indexes: []
});
module.exports=Sales;