const { Sequelize } = require("sequelize");
var OrderTickets = sequelize.define(
  "order_tickets",
  {
    id: {
      type: Sequelize.BIGINT,
      field: "id",
      primaryKey: true,
      autoIncrement: true,
      allowNull: false,
    },
    code: {
      type: Sequelize.STRING(255),
      field: "code",
      unique: true,
      allowNull: false,
    },
    date_order: {
      type: Sequelize.DATE,
      field: "date_order",
      allowNull: true,
    },
    amount_room: {
      type: Sequelize.BIGINT,
      field: "amount_room",
      allowNull: false,
    },
    amount_date: {
      type: Sequelize.BIGINT,
      field: "amount_date",
    },
    date_welcome: {
      type: Sequelize.DATE,
      field: "date_welcome",
      allowNull: true,
    },
    date_leave: {
      type: Sequelize.DATE,
      field: "date_leave",
      allowNull: true,
    },
    status: {
      type: Sequelize.STRING(255),
      field: "status",
      allowNull: true,
    },
    createdAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
    updatedAt: {
      type: Sequelize.DATE,
      field: "createdAt",
      allowNull: true,
    },
  },
  {
    freezeTableName: true,
    indexes: [],
  }
);
module.exports = OrderTickets;
