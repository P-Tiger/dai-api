global.Sequelize = require("sequelize");
global.sequelize = new Sequelize("hotel_manager", "root", "mixaolanot123", {
  host: "127.0.0.1",
  //logging: false,
  dialect: "mysql",
  pool: {
    max: 50,
    min: 0,
    idle: 10000,
  },
});

// TABLES

global.Part = require("./part.js");
global.Customers = require("./customers");
global.Bills = require("./bills");
global.DetailBills = require("./detail_bills");
global.DetailCustomerAts = require("./detail_customer_ats");
global.DetailOrderTickets = require("./detail_order_tickets");
global.DetailSales = require("./detail_sales");
global.DetailServices = require("./detail_serviec");
global.KindRooms = require("./kind_room");
global.OrderTickets = require("./order_ticket");
global.RankRooms = require("./rank_room");
global.Rents = require("./rents");
global.RentTickets = require("./rent_tickets");
global.Rooms = require("./rooms");
global.Sales = require("./sales");
global.Services = require("./services");
global.Staffs = require("./staffs");
global.StatusRooms = require("./status_room");
global.TypeRooms = require("./type_room");
global.DetailStatus=require("./detail_status")
//join detail order
OrderTickets.hasMany(DetailOrderTickets, {
  sourceKey: "id",
  foreignKey: "order_ticket_id",
  as: "detail_list_detail_order",
});
DetailOrderTickets.belongsTo(OrderTickets, {
  targetKey: "id",
  foreignKey: "order_ticket_id",
  as: "detail_info_order",
});
RankRooms.hasMany(DetailOrderTickets, {
  sourceKey: "id",
  foreignKey: "rank_room_id",
  as: "detail_list_detail_order",
});
DetailOrderTickets.belongsTo(RankRooms, {
  targetKey: "id",
  foreignKey: "rank_room_id",
  as: "detail_info_rank_room",
});
//join rank room
KindRooms.hasMany(RankRooms, {
  sourceKey: "id",
  foreignKey: "kind_room_id",
  as: "detail_list_rank_room",
});
RankRooms.belongsTo(KindRooms, {
  targetKey: "id",
  foreignKey: "kind_room_id",
  as: "detail_info_kind_room",
});
TypeRooms.hasMany(RankRooms, {
  sourceKey: "id",
  foreignKey: "type_room_id",
  as: "detail_list_rank_room_type",
});
RankRooms.belongsTo(TypeRooms, {
  targetKey: "id",
  foreignKey: "type_room_id",
  as: "detail_info_type_room",
});
//join customer ,order,staff
Customers.hasMany(OrderTickets, {
  sourceKey: "id",
  foreignKey: "customer_id",
  as: "detail_list_order",
});
OrderTickets.belongsTo(Customers, {
  targetKey: "id",
  foreignKey: "customer_id",
  as: "detail_info_customer",
});
Staffs.hasMany(OrderTickets, {
  sourceKey: "id",
  foreignKey: "staff_id",
  as: "detail_list_order",
});
OrderTickets.belongsTo(Staffs, {
  targetKey: "id",
  foreignKey: "staff_id",
  as: "detail_info_staff",
});
//join staff ,part
Part.hasMany(Staffs, {
  sourceKey: "id",
  foreignKey: "part_id",
  as: "detail_list_staff",
});
Staffs.belongsTo(Part, {
  targetKey: "id",
  foreignKey: "part_id",
  as: "detail_info_part",
});
//Rooms
RankRooms.hasMany(Rooms, {
  sourceKey: "id",
  foreignKey: "rank_room_id",
  as: "detail_list_room",
});
Rooms.belongsTo(RankRooms, {
  targetKey: "id",
  foreignKey: "rank_room_id",
  as: "detail_info_rankroom",
});
//detail room
Rooms.hasMany(DetailStatus,{
  sourceKey: "id",
  foreignKey: "rooms_id",
  as: "detail_list_status",
})
DetailStatus.belongsTo(Rooms, {
  targetKey: "id",
  foreignKey: "rooms_id",
  as: "detail_info_room",
});
StatusRooms.hasMany(DetailStatus,{
  sourceKey: "id",
  foreignKey: "status_rooms_id",
  as: "detail_list_detailstatus",
})
DetailStatus.belongsTo(StatusRooms, {
  targetKey: "id",
  foreignKey: "status_rooms_id",
  as: "detail_info_status",
});
OrderTickets.hasMany(DetailStatus,{
  sourceKey: "id",
  foreignKey: "order_tickets_id1",
  as: "detail_list_detailstatus",
})
DetailStatus.belongsTo(OrderTickets,{
  targetKey: "id",
  foreignKey: "order_tickets_id1",
  as: "detail_info_order",
})
//rent-ticket
OrderTickets.hasMany(RentTickets, {
  sourceKey: "id",
  foreignKey: "order_ticket_id",
  as: "detail_list_rent",
});
RentTickets.belongsTo(OrderTickets, {
  targetKey: "id",
  foreignKey: "order_ticket_id",
  as: "detail_info_order",
});
Staffs.hasMany(RentTickets, {
  sourceKey: "id",
  foreignKey: "staff_id",
  as: "detail_list_rent",
});
RentTickets.belongsTo(Staffs, {
  targetKey: "id",
  foreignKey: "staff_id",
  as: "detail_info_staff",
});
Customers.hasMany(RentTickets, {
  sourceKey: "id",
  foreignKey: "customer_id",
  as: "detail_list_rent",
});
RentTickets.belongsTo(Customers, {
  targetKey: "id",
  foreignKey: "customer_id",
  as: "detail_info_customer",
});
//rent
RentTickets.hasMany(Rents, {
  sourceKey: "id",
  foreignKey: "rent_ticket_id",
  as: "detail_list_rents",
});
Rents.belongsTo(RentTickets, {
  targetKey: "id",
  foreignKey: "rent_ticket_id",
  as: "detail_info_rent_ticket",
});
Rooms.hasMany(Rents, {
  sourceKey: "id",
  foreignKey: "room_id",
  as: "detail_list_rents",
});
Rents.belongsTo(Rooms, {
  targetKey: "id",
  foreignKey: "room_id",
  as: "detail_info_rooms",
});
//detail customer at
Customers.hasMany(DetailCustomerAts, {
  sourceKey: "id",
  foreignKey: "customers_id",
  as: "detail_list_detail_rent",
});
DetailCustomerAts.belongsTo(Customers, {
  targetKey: "id",
  foreignKey: "customers_id",
  as: "detail_info_customer",
});
Rents.hasMany(DetailCustomerAts, {
  sourceKey: "id",
  foreignKey: "rent_id",
  as: "detail_list_detail_rent",
});
DetailCustomerAts.belongsTo(Rents, {
  targetKey: "id",
  foreignKey: "rent_id",
  as: "detail_info_rent",
});
//detal service
Services.hasMany(DetailServices, {
  sourceKey: "id",
  foreignKey: "service_id",
  as: "detail_list_detail_service",
});
DetailServices.belongsTo(Services, {
  targetKey: "id",
  foreignKey: "service_id",
  as: "detail_info_service",
});
Rents.hasMany(DetailServices, {
  sourceKey: "id",
  foreignKey: "rent_id",
  as: "detail_list_detail_service",
});
DetailServices.belongsTo(Rents, {
  targetKey: "id",
  foreignKey: "rent_id",
  as: "detail_info_rent",
});
//detail sale
Sales.hasMany(DetailSales, {
  sourceKey: "id",
  foreignKey: "sale_id",
  as: "detail_list_detail_sale",
});
DetailSales.belongsTo(Sales, {
  targetKey: "id",
  foreignKey: "sale_id",
  as: "detail_info_sale",
});
Rooms.hasMany(DetailSales, {
  sourceKey: "id",
  foreignKey: "room_id",
  as: "detail_list_detail_sale",
});
DetailSales.belongsTo(Rooms, {
  targetKey: "id",
  foreignKey: "room_id",
  as: "detail_info_room",
});
//bill
Staffs.hasMany(Bills, {
  sourceKey: "id",
  foreignKey: "staff_id",
  as: "detail_list_detail_bill",
});
Bills.belongsTo(Staffs, {
  targetKey: "id",
  foreignKey: "staff_id",
  as: "detail_info_staff",
});
Customers.hasMany(Bills, {
  sourceKey: "id",
  foreignKey: "customer_id",
  as: "detail_list_detail_bill",
});
Bills.belongsTo(Customers, {
  targetKey: "id",
  foreignKey: "customer_id",
  as: "detail_info_customer",
});
RentTickets.hasMany(Bills, {
  sourceKey: "id",
  foreignKey: "rent_ticket_id",
  as: "detail_list_detail_bill",
});
Bills.belongsTo(RentTickets, {
  targetKey: "id",
  foreignKey: "rent_ticket_id",
  as: "detail_info_rent_ticket",
});
//detail bill
Bills.hasMany(DetailBills, {
  sourceKey: "id",
  foreignKey: "bill_id",
  as: "detail_list_detail_detail_bill",
});
DetailBills.belongsTo(Bills, {
  targetKey: "id",
  foreignKey: "bill_id",
  as: "detail_info_bill",
});
Rents.hasMany(DetailBills, {
  sourceKey: "id",
  foreignKey: "rent_id",
  as: "detail_list_detail_detail_bill",
});
DetailBills.belongsTo(Rents, {
  targetKey: "id",
  foreignKey: "rent_id",
  as: "detail_info_rent",
});
